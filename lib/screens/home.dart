import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:fluttertube/blocs/favorite_bloc.dart';
import 'package:fluttertube/blocs/videos_bloc.dart';
import 'package:fluttertube/delegates/data_search.dart';
import 'package:fluttertube/models/video.dart';
import 'package:fluttertube/screens/favorites_screen.dart';
import 'package:fluttertube/tile/video_tile.dart';


class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final bloc = BlocProvider.getBloc<VideosBloc>();
    final bloc2 = BlocProvider.getBloc<FavoriteBloc>();

    return Scaffold(
      appBar: AppBar(
        title: Container(
          height: 45,
          child: Image.asset("images/logo-youtube-dark.jpg"),
        ),
        elevation: 0,
        backgroundColor: Colors.black87,
        actions: <Widget>[
          
          Align(
            alignment: Alignment.center,
            child: StreamBuilder<Map<String, Video>>
              (
                stream: bloc2.outFav,
                builder: (context, snapshot){
                  if(snapshot.hasData) return Text("${snapshot.data.length}");
                  else return Container();
                }
            ),
          ),
          
          IconButton(
              icon: Icon(Icons.star),
              onPressed: (){
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => Favorites())
                );
              }
          ),

          IconButton(
              icon: Icon(Icons.search),
              onPressed: () async{
                //Result recebe o valor de pesquisa(query) da DataSearch
               String result = await showSearch(context: context, delegate: DataSearch());
               //Adiciona o termo da pesquisa(result) que vem da searchDelegate
               if(result != null) BlocProvider.getBloc<VideosBloc>().inSearch.add(result);
              }
          ),
        ],
      ),
      backgroundColor: Colors.black,
      //StreamBuilder para renderizar a tela quando mudar o valor da Stream
      body: StreamBuilder(
        stream: BlocProvider.getBloc<VideosBloc>().outVideos,
          initialData: [],
          builder: (context, snapshot){
            if(snapshot.hasData)
              return ListView.builder(
                //Lista sempre vai achar que tem mais um
                itemCount: snapshot.data.length + 1,
                  itemBuilder: (context,index){
                  //Vai verificar se ainda está em um número válido e não no item falso(+1)
                    //Quando chegar no 10 ele vai começar a procurar um item que não existe
                    if(index < snapshot.data.length){
                      return VideoTile(snapshot.data[index]);
                      //tenta carregar o último da nossa lista, o item falso (+1)
                      //Para verificar se já existe dados na lista ou se é apenas o item falso, se iniciar com sem essa verificação
                      //A lista entrará em um loop eterno já que sempre vai ter esse valor falso que carrega mais vídeos
                    }else if(index > 1){
                      //Envia o nulo para pesquisa
                      bloc.inSearch.add(null);
                      return Container(
                        height: 40.0,
                        width: 40.0,
                        alignment: Alignment.center,
                        child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation(Colors.red),),
                      );
                    }else{
                      return Container();
                    }
                  }
              );
            else
              return Container();
          }
      ),
    );
  }
}
