class Video{
  //final pois depois que definidos eles não mudam mais
  final String id;
  final String title;
  //Imagem do video
  final String thumb;
  final String channel;

  Video({this.id, this.title, this.thumb, this.channel});


  //Pega Json e tranforma em objeto Video
  factory Video.fromJson(Map<String, dynamic> json){
    //Verifica se o json está no formato google ou no feito no app
    if(json.containsKey('id'))
    return Video(
      id: json["id"]["videoId"],
      title: json["snippet"]["title"],
      thumb: json["snippet"]["thumbnails"]["high"]["url"],
      channel: json["snippet"]["channelTitle"]
    );
    else
      return Video(
        id: json["videoId"],
        title:  json["title"],
        thumb: json["thumb"],
        channel: json["channel"]
      );
  }


  Map<String, dynamic> toJson(){
    return{
      "videoId" : id,
      "title" : title,
      "thumb" : thumb,
      "channel" : channel
    };
  }

}