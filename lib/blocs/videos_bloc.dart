import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:fluttertube/apis/api.dart';
import 'package:fluttertube/models/video.dart';
import 'dart:async';


class VideosBloc implements BlocBase{

  Api api;
  List<Video> videos;
  final StreamController<List<Video>> _videosController = StreamController<List<Video>>();
  //Esse vai enviar dados para fora, não receber
  Stream get outVideos => _videosController.stream;

  final StreamController<String> _searchController = StreamController<String>();
  //Esse controler vai receber dados de fora mas não vai enviar
  Sink get inSearch => _searchController.sink;

  VideosBloc(){
    api= Api();

    //Recebe o método search como parametro
    _searchController.stream.listen(_search);
  }

  void _search(String search) async{
    if(search != null){
      //Enviar Lista vazia para que apresente a lista inicial quando outro termo for pesquisado
      _videosController.sink.add([]);

      //Realiza pesquisa com o termo recebido da home
      videos = await api.search(search);
    }else{
      videos+= await api.nextPage();
    }

    //Depois de receber os dados da pesquisa na lista de videos, é adicionado essa lista ao Videos Controller
    _videosController.sink.add(videos);

  }

  @override
  void addListener(listener) {
    // TODO: implement addListener
  }

  @override
  void dispose() {
    _videosController.close();
    _searchController.close();
  }

  @override
  // TODO: implement hasListeners
  bool get hasListeners => null;

  @override
  void notifyListeners() {
    // TODO: implement notifyListeners
  }

  @override
  void removeListener(listener) {
    // TODO: implement removeListener
  }

}